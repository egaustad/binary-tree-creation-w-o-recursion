# README #

Created by Erik Gaustad as a practice exercise.

This is a C++ binary tree creation class without using recursion.

Recursion is only used in the print function to print the nodes in order.

Creation of the binary tree is made simple with recursion so it was a better exercise to create it the hard way.