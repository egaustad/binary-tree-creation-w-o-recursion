//
// Created by Erik Gaustad
//
#include <stdio.h>

#ifndef BINARYTREE_BINARY_TREE_H
#define BINARYTREE_BINARY_TREE_H
class BinaryTree{
private:
    struct node {
        int key;
        struct node *left, *right;
    };
    struct node* root;
    //Print function to print the nodes in order from smallest to largest with recursion
    void Print_Tree(struct node * node){
        if (node != nullptr){
            Print_Tree(node->left);
            printf("%d / ",node->key);
            Print_Tree(node->right);
        }
    }
    //Insert a node into the tree without recursion
    void Insert(struct node * new_node){
        if (root == nullptr){
            root = new_node;
            return;
        }else{
            struct node * current = root;
            struct node * previous;
            while (current != nullptr){
                previous = current;
                if (new_node->key < current->key){
                    current = current->left;
                }
                else{
                    current = current->right;
                }
            }
            if (new_node->key < previous->key){
                previous->left = new_node;
            }
            else{
                previous->right = new_node;
            }
        }
    }
    //Delete each node in the tree with recursion and print each node being deleted
    void DeleteTree(struct node * node){
        if (node!= nullptr){
            DeleteTree(node->left);
            DeleteTree(node->right);
            printf("Deleting: %d\n",node->key);
            delete node;
        }
    }
public:
    //CTOR
    BinaryTree (){
        root = nullptr;
    }
    //DTOR
    ~BinaryTree(){
        DeleteTree(root);
    };
    //Public: Create a new node and call the insert function
    void AddNode(int new_key){
        struct node * temp = new node();
        temp->key = new_key;
        temp->left = temp->right = nullptr;
        Insert(temp);
    }
    //Loop through and find the lowest left node in the tree
    void PrintMinimumValue(){
        struct node * current = root;
        while(current->left != nullptr){
            current = current->left;
        }
        printf("The minimum value in the tree is: %d\n", current->key);
    }
    //Delete the node with given key without recursion
    bool DeleteNode(int key){
        if (root == nullptr){
            printf("The tree is empty.\n");
            return false;
        }
        else{
            struct node * current = root;
            struct node * previous;
            //loop through the tree to find the node to be deleted
            while (current->key != key && current != nullptr){
                previous = current;
                if (key < current->key){
                    current = current->left;
                }else{
                    current = current->right;
                }
            }
            if (current->key == key){
                //if both branch nodes are nullptr then deletion is simple
                if (current->left == nullptr && current->right == nullptr){
                    //Tree is now empty if the root has the key
                    if (key == root->key){
                        delete root;
                        return true;
                    }
                    else{
                        //if the root isn't the key then set the previous node to nullptr because no branch nodes follow
                        if (key < previous->key){
                            previous->left = nullptr;
                            delete current;
                        }
                        else{
                            previous->right = nullptr;
                            delete current;
                        }
                    }
                }
                //if the left branch is nullptr
                else if(current->left == nullptr){
                    if (key < previous->key){
                        previous->left = current->right;
                        delete current;
                        return true;
                    }
                    else{
                        previous->right = current->right;
                        delete current;
                        return true;
                    }
                }
                //if the right branch is nullptr
                else if(current->right == nullptr){
                    if (key < previous->key){
                        previous->left = current->left;
                        delete current;
                        return true;
                    }
                    else{
                        previous->right = current->left;
                        delete current;
                        return true;
                    }
                }
                //if both branches have nodes
                else{
                    struct node * temp = current->right;
                    struct node * previous_temp;
                    //find the right successor which is the lowest value node on the right side of current node
                    while (temp->left != nullptr){
                        previous_temp = temp;
                        temp = temp->left;
                    }
                    //set the previous node to point at the following node so deletion of the successor can happen
                    if (temp->right != nullptr){
                        previous_temp->left = temp->right;
                        if (key < previous->key){
                            previous->left = temp;
                            temp->left = current->left;
                            temp->right = current->right;
                            delete current;
                            return true;
                        }
                        else{
                            previous->right = temp;
                            temp->left = current->left;
                            temp->right = current->right;
                            delete current;
                            return true;
                        }
                    }
                    //if there is no node to the right of successor node then replace node to be deleted with successor
                    else{
                        previous_temp->left = nullptr;
                        if (key < previous->key){
                            previous->left = temp;
                            temp->left = current->left;
                            temp->right = current->right;
                            delete current;
                            return true;
                        }
                        else{
                            previous->right = temp;
                            temp->left = current->left;
                            temp->right = current->right;
                            delete current;
                            return true;
                        }
                    }
                }
            }
            else{
                printf("Couldn't find value: %d\n",key);
                return false;
            }
        }
    }
    void PrintTree (){
        Print_Tree(root);
    }
};
#endif //BINARYTREE_BINARY_TREE_H
