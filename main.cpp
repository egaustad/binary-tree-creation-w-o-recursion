#include "binary_tree.h"
#include <array>

using namespace std;
int main() {
    BinaryTree *btree = new BinaryTree();

    int array1 [] = {50,95,45,15,40,105,55,30,20,35,70,60,65,80};

    for (unsigned i = 0; i < 14; i ++){
        btree->AddNode(array1[i]);
    }
    printf("Tree printing in order from smallest to largest:\n");
    btree->PrintTree();
    printf("\n");
    btree->PrintMinimumValue();
    btree->DeleteNode(45);
    btree->AddNode(10);
    printf("Tree printing in order from smallest to largest after change:\n");
    btree->PrintTree();
    printf("\n");
    btree->PrintMinimumValue();

    delete btree;

    return 0;
}